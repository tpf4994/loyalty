import java.awt.*;
import java.awt.event.KeyEvent;
import javax.swing.*;
import java.util.Random;
import java.awt.geom.GeneralPath;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

///the main game is here
public class PanelPelota extends JPanel implements Runnable {

//this is the complete code

private static final long serialVersionUID = 1L;
// Positions on X and Y for the ball, player 1 and player 2

//for debugging
float predictedPos=1;
int move =0;
boolean rect = true ;

// computer intelligence
int countPeriod =0 ;
int countRethinking =0;
int period = 0;
int rethinkPeriod = 1000000 ;
boolean token = false;

// is ball moving right or left ?
boolean movingRight=false;
boolean movingUp=false;

// the initial co-ordinates of ball and players(4)
private int ballX = 300, ballY = 30, player2X=0, player2Y=100, player4X=485, player4Y=100,player1X=200, player1Y=462,player3X=200, player3Y=0;

boolean[] active = {true,true,true,true} ; // are the players dead or active ?
boolean spin =false ;

int currentlyDead=0;

//bonus 
boolean isBonusVisible = false ;
int bonusX , bonusY ,bonusVisibleTime=0,bonusOneTime=0,bonusTwoTime=0,bonusThreeTime=0,bonusFourTime=0;
int bonusMode =1 ;// can take values  1 ,2 ,3 ,4 telling which player can currently avail bonus 
boolean isBonusOne=false ,isBonusTwo=false,isBonusThree=false ,isBonusFour=false ;


//are the players computer operated or manual
boolean isComputerOne =false ;boolean isComputerTwo=true; boolean isComputerThree=true; boolean isComputerFour=true;

String win ="";

Thread hilo;

int playerSpeed = 1;
int bonusSpeed = 2 ;

int playerUp = playerSpeed;
int playerDown = -playerSpeed;
int playerRight = playerSpeed;
int playerLeft = -playerSpeed;

int BonusUp = bonusSpeed;
int BonusDown = -bonusSpeed;
int BonusRight = bonusSpeed;
int BonusLeft = -bonusSpeed;

int ballSpeed = 1;
// the player speeds and ball speeds are specigied differently
int right=ballSpeed; // to the right
int left= -ballSpeed; //to the left
int up=ballSpeed; // upward
int down= -ballSpeed; // down
int width, height; // Width and height of the screen
// Scores
int contPlay1=0, contPlay2=0 ,contPlay3=0, contPlay4=0;

// game state tokens
boolean gameInitiate = false;
boolean gameStart = false;



// what keys are pressed for the different players
boolean player4Down,player4Up, player2Down, player2Up,player3Right, player3Left,player1Right, player1Left;


// random generator for the bonus points
Random rn = new Random();

boolean gameOn, gameOver;

public PanelPelota(){
gameOn=true;
hilo=new Thread(this);
hilo.start();
}

// Draw ball and ships
public void paintComponent(Graphics gc){
setOpaque(false);
super.paintComponent(gc);

// Draw ball
gc.setColor(Color.black);
gc.fillOval(ballX, ballY, 8,8);
//gc.fillRect(230, 245, 10, 25);

//Draw corners
gc.fillRect(0, 0 , 10 , 10);
gc.fillRect(485, 0 , 10 , 10);
gc.fillRect(485, 462 , 10 , 10);
gc.fillRect(0, 462 , 10 , 10);



// Draw ships

gc.setColor(Color.blue);
if (isBonusOne)gc.setColor(Color.green);
gc.fillRect(player1X, player1Y, 70, 10);
gc.setColor(Color.red);
if (isBonusTwo)gc.setColor(Color.green);
gc.fillRect(player2X, player2Y, 10, 70);
gc.setColor(Color.blue);
if (isBonusThree)gc.setColor(Color.green);
gc.fillRect(player3X, player3Y, 70, 10);
gc.setColor(Color.red);
if (isBonusFour)gc.setColor(Color.green);
gc.fillRect(player4X, player4Y, 10, 70);

// draw bonus icon  which is randomply generated
if (isBonusVisible)
{
	gc.setColor(Color.green);
	gc.fillOval(bonusX, bonusY, 15,10);
}
gc.setColor(Color.red);
//Draw scores and print things for debugging
if (rect&&contPlay4==5) {contPlay4=1; rect=false;}
 gc.setColor(Color.black);
gc.drawString( ""+contPlay1, player1X+30,player1Y+7);
gc.drawString(""+contPlay2, player2X, player2Y +30);
gc.drawString(""+contPlay3, player3X+30, player3Y+10);
gc.drawString(""+contPlay4, player4X, player4Y+30);

/*gc.drawString("pos :"+predictedPos, 220, 10);
gc.drawString("move "+move, 300, 10);
gc.drawString("Player1Y "+player1Y, 400, 10);

gc.drawString("moving right" +movingRight, 25, 450);
gc.drawString("movingUp "+movingUp, 150, 450);
gc.drawString("up :"+up, 220, 450);
gc.drawString("down "+down, 300, 450);

gc.drawString("ballY "+ballY, 300, 400);
gc.drawString("movingUp "+movingUp, 400, 400);
gc.drawString("2X "+player2X, 200, 400);
gc.drawString("2Y "+player2Y, 100, 400);*/



if(gameOver)
gc.drawString("Game Over: "+win, 200, 200);
}

// Positions on X and Y for the ball
public void updateBall (int nx, int ny)
{
ballX= nx;
ballY= ny;
this.width=this.getWidth();
this.height=this.getHeight();
repaint();
}

// Here we receive from the game container class the key pressed
public void keyPressed(KeyEvent evt){

	// looking at which players are manual , corresponding states are alloted to those players loking at the key presses 
	
	if (!isComputerOne)
	{
		switch(evt.getKeyCode())
		{
			case KeyEvent.VK_LEFT:
			player1Left=true;
			break;

			case KeyEvent.VK_RIGHT:
			player1Right=true;
			break;
		}
	}
	
	if (!isComputerTwo)
	{
		switch(evt.getKeyCode())
		{
			case KeyEvent.VK_UP:
			player2Down=true;
			break;

			case KeyEvent.VK_DOWN:
			player2Up=true;
			break;
		}
	}
	
	if (!isComputerThree)
	{
		switch(evt.getKeyCode())
		{
			case KeyEvent.VK_LEFT:
			player3Left=true;
			break;

			case KeyEvent.VK_RIGHT:
			player3Right=true;
			break;
		}
	}
	
	if (!isComputerFour)
	{
		switch(evt.getKeyCode())
		{
			case KeyEvent.VK_UP:
			player4Down=true;
			break;

			case KeyEvent.VK_DOWN:
			player4Up=true;
			break;
		}
	}
	
	
}

// Here we receive from the game container class the key released
public void keyReleased(KeyEvent evt)
{
if (!isComputerOne)
	{
		switch(evt.getKeyCode())
		{
			case KeyEvent.VK_LEFT:
			player1Left=false;
			break;

			case KeyEvent.VK_RIGHT:
			player1Right=false;
			break;
		}
	}
	
	if (!isComputerTwo)
	{
		switch(evt.getKeyCode())
		{
			case KeyEvent.VK_UP:
			player2Down=false;
			break;

			case KeyEvent.VK_DOWN:
			player2Up=false;
			break;
		}
	}
	
	if (!isComputerThree)
	{
		switch(evt.getKeyCode())
		{
			case KeyEvent.VK_LEFT:
			player3Left=false;
			break;

			case KeyEvent.VK_RIGHT:
			player3Right=false;
			break;
		}
	}
	
	if (!isComputerFour)
	{
		switch(evt.getKeyCode())
		{
			case KeyEvent.VK_UP:
			player4Down=false;
			break;

			case KeyEvent.VK_DOWN:
			player4Up=false;
			break;
		}
	}
	
}

// Move player 1


public void moverPlayer1()
{ 
		if (player1Left == true && player1X >= 10)
			player1X += playerLeft;
		if (player1Right == true && player1X <= (this.getWidth()-80))
			player1X += playerRight;
	updatePlayer1(player1X, player1Y);
	
}

// Move player 2
public void moverPlayer2()
{
	
		if (player2Down == true && player2Y >= 10)
			player2Y += playerDown;
		if (player2Up == true && player2Y <= (this.getHeight()-80))
			player2Y += playerUp;
		updatePlayer2(player2X, player2Y);
	
}

// Move player 3
public void moverPlayer3()
{
 
	if (player3Left == true && player3X >= 10)
		player3X += playerLeft;
	if (player3Right == true && player3X <= (this.getWidth()-80))
		player3X += playerRight;
	updatePlayer3(player3X, player3Y);
	
}
// Move player 4
public void moverPlayer4()
{
   
		if (player4Down == true && player4Y >= 10)
			player4Y += playerDown;
		if (player4Up == true && player4Y <= (this.getHeight()-80))
			player4Y += playerUp;
		updatePlayer4(player4X, player4Y);
		
	
}



// Position on Y for the player 1
public void updatePlayer1(int x, int y){
this.player1X=x;
this.player1Y=y;
repaint();
}
// Position on Y for the player 2
public void updatePlayer2(int x, int y){
this.player2X=x;
this.player2Y=y;
repaint();
}
// Position on Y for the player 3
public void updatePlayer3(int x, int y){
this.player3X=x;
this.player3Y=y;
repaint();
}
// Position on Y for the player 4
public void updatePlayer4(int x, int y){
this.player4X=x;
this.player4Y=y;
repaint();
}


public void run() {
// TODO Auto-generated method stub

while(true){

if(gameOn){

// The ball move from left to right
if (movingRight)
{
// a la right
ballX += right;
if (ballX >= 484)
movingRight= false;
}
else
{
// a la left
ballX += left;
if ( ballX <= 0)
movingRight = true;
}


// The ball moves from up to down
if (movingUp)
{
// hacia up
ballY += up;
if (ballY >= (464))
movingUp= false;

}
else
{
// hacia down
ballY += down;
if ( ballY <= 0)
movingUp = true;
}
updateBall(ballX, ballY);

// Delay
try
{
Thread.sleep(5);
}
catch(InterruptedException ex)
{

}

// handling bonus mode 

// if a player already has bonus , countPeriod his time 
if (isBonusOne){
	if (bonusOneTime==600){
	   isBonusOne =false;
	}
	else {
		bonusOneTime++;
	}
}
/////////
if (isBonusTwo){
	if (bonusTwoTime==600){
	   isBonusTwo =false;
	}
	else {
		bonusTwoTime++;
	}
}
///////////
if (isBonusThree){
	if (bonusThreeTime==600){
	   isBonusThree =false;
	}
	else {
		bonusThreeTime++;
	}
}
///////////
if (isBonusFour){
	if (bonusFourTime==600){
	   isBonusFour =false;
	}
	else {
		bonusFourTime++;
	}
}

//handling bonus visibility 
if (isBonusVisible){
	
	if (bonusMode==1){
		if(bonusX<(player1X+80) && bonusX>(player1X-5))
		{isBonusVisible = false;
	      isBonusOne = true ;
		  if (contPlay1<6 && contPlay1>0)
		  contPlay1--;
		  bonusOneTime = 0;
		}
		else {
			bonusVisibleTime++;
			if (bonusVisibleTime==600)
				isBonusVisible = false;
		}
	}
	else if (bonusMode==2){
		if(bonusY<(player2Y+80) && bonusY>(player2Y-5))
		{isBonusVisible = false;
	      isBonusTwo = true ;
		  if (contPlay2<6&& contPlay2>0)
		  contPlay2--;
		  bonusTwoTime = 0;
		}
		else {
			bonusVisibleTime++;
			if (bonusVisibleTime==600)
				isBonusVisible = false;
		}
	}
	else if (bonusMode==3){
		if(bonusX<(player3X+80) && bonusX>(player3X-5))
		{isBonusVisible = false;
	      isBonusThree = true ;
		  if (contPlay3<6 && contPlay3>0)
		  contPlay3--;
		  bonusThreeTime = 0;
		}
		else {
			bonusVisibleTime++;
			if (bonusVisibleTime==600)
				isBonusVisible = false;
		}
	}
	else 
		{
		if(bonusY<(player4Y+80) && bonusY>(player4Y-5))
		{isBonusVisible = false;
	      isBonusFour = true ;
		  if (contPlay4<6 &&  contPlay4>0)
		  contPlay4--;
		  bonusFourTime = 0;
		}
		else {
			bonusVisibleTime++;
			if (bonusVisibleTime==600)
				isBonusVisible = false;
			}
		}
}

else 
		{
			if (rn.nextInt(100)==25 && !isBonusFour && !isBonusOne && !isBonusThree && !isBonusTwo)
			{
				isBonusVisible =true;
				bonusVisibleTime=0;
				bonusMode = rn.nextInt(4) +1 ;
				
				if (bonusMode==1){
					bonusY = player1Y;
					bonusX = rn.nextInt(450);
					while (bonusX<(player1X+80) && bonusX>(player1X-5) && bonusX < 20){
						bonusX = rn.nextInt(480);
					}
					
				}
				else if (bonusMode==2){
					bonusX = player2X;
					bonusY = rn.nextInt(440);
					while (bonusY<(player2Y+80) && bonusY>(player2Y-5) && bonusY <20)
						bonusY = rn.nextInt(440);
				}
				else if (bonusMode==3){
					bonusY = player3Y;
					bonusX = rn.nextInt(450);
					while (bonusX<(player3X+80) && bonusX>(player3X-5) && bonusX < 20){
						bonusX = rn.nextInt(480);
					}
					
				}
				else {
					bonusX = player4X;
					bonusY = rn.nextInt(440);
					while (bonusY<(player4Y+80) && bonusY>(player4Y-5) && bonusY <20)
						bonusY = rn.nextInt(440);
					
				}
			}
	 
		}





//HERE , for all those computer operated players , the next move is fetched from the computerEasy CLASS  by passing the required parameters
  
  
  if (isComputerOne){
// predictedPos = computerEasy.giveNextPos(2,player1Y,ballX,ballY,movingRight,!movingUp,-1*down,up,right,-1*left);
	if (countPeriod==period){
		move = computerEasy.giveNextMove(1,player1X,ballX,ballY,movingRight,!movingUp,-1*down,up,right,-1*left);
        
		if (countRethinking==rethinkPeriod) {move = -1*move ; countRethinking=0;}
		else countRethinking++;
		
		if (move==1) {player1Left = false;player1Right=true;} 
		else if (move==0) {player1Left = false;player1Right=false;} 
		else if (move==-1) {player1Left = true;player1Right=false;} 
		countPeriod=0;
	}
	else  {player1Left = false;player1Right=false; countPeriod++;} 
    		
  
  } 
  
  if (isComputerTwo){
// predictedPos = computerEasy.giveNextPos(2,player1Y,ballX,ballY,movingRight,!movingUp,-1*down,up,right,-1*left);
 if (countPeriod==period){
			move = computerEasy.giveNextMove(2,player2Y,ballX,ballY,movingRight,!movingUp,-1*down,up,right,-1*left);

			if (countRethinking==rethinkPeriod) {move = -1*move ; countRethinking=0;}
			else countRethinking++;
			
			if (move==1) {player2Down = false;player2Up=true;} 
			else if (move==0) {player2Down = false;player2Up=false;}
			else if (move==-1) {player2Down = true;player2Up=false;}
			countPeriod=0;}
	else 	{player2Down = false;player2Up=false;countPeriod++;}	
  
  } 

   if (isComputerThree){
// predictedPos = computerEasy.giveNextPos(2,player1Y,ballX,ballY,movingRight,!movingUp,-1*down,up,right,-1*left);
	if (countPeriod==period)
	{
			move = computerEasy.giveNextMove(3,490-player3X,ballX,ballY,movingRight,!movingUp,-1*down,up,right,-1*left);
			
			if (countRethinking==rethinkPeriod){move = -1*move ; countRethinking=0;}
			else countRethinking++;

			if (move==1) {player3Left = true;player3Right=false;} 
			else if (move==0) {player3Left = false;player3Right=false;} 
			else if (move==-1) {player3Left = false;player3Right=true;} 
			countPeriod=0;
	}
	else {player3Left = false;player3Right=false;countPeriod++;} 
  } 
  
   if (isComputerFour){
// predictedPos = computerEasy.giveNextPos(2,player1Y,ballX,ballY,movingRight,!movingUp,-1*down,up,right,-1*left);
	if(countPeriod==period)
	{
			move = computerEasy.giveNextMove(4,460-player4Y,ballX,ballY,movingRight,!movingUp,-1*down,up,right,-1*left);
			
			if (countRethinking==rethinkPeriod) {move = -1*move ; countRethinking=0;}
			else countRethinking++;

			if (move==1) {player4Down = true; player4Up=false;} 
			else if (move==0) {player4Down = false;player4Up=false;}
			else if (move==-1) {player4Down = false;player4Up=true;}
			countPeriod=0;
	}
	else {player4Down = false;player4Up=false;countPeriod++;}
  } 
  
// Move player 1
if (contPlay1<6)
moverPlayer1();

// Move player 2
if (contPlay2<6)
moverPlayer2();

// Move player 3
if (contPlay3<6)
moverPlayer3();

// Move player 4
if (contPlay4<6)
moverPlayer4();

// The countPeriod of the player 4 increase
if (ballX >= (482)&&contPlay4<6){
contPlay4++;token =true;}

// The countPeriod of the player 1 increase
if (ballY >= (464)&&contPlay1<6)
contPlay1++;

// The countPeriod of the player 3 increase
if (ballY <= 0&&contPlay3<6  )
contPlay3++;

// The countPeriod of the player 2 increase
if ( ballX == 0&&contPlay2<6)
contPlay2++;

// Game over. Here you can change 6 to any value
// When the score reach to the value, the game will end
if(contPlay1==6 ){
	active[0]=false;
}

if(contPlay2==6 ){
	active[1] = false;
}
if(contPlay4==6 ){
	active[3] = false;
}

if(contPlay3==6 ){
	active[2] = false;
}

for (int i=0 ;i<4 ;i++){
	int j = i+1;
	if (active[i]==false) currentlyDead++;
	else win = "PLAYER "+j+" WINS";
}
if (currentlyDead==3){
	gameOn=false; gameOver=true;
}
else currentlyDead =0;


// Left side collision with the first player 
if (ballY>=453 && ballX<(player1X-3) && ballX>(player1X-5) &&movingRight )
{
	movingUp = false;
	movingRight=false;
	if (contPlay1<6)
	contPlay1++;
}

//Right side collision with the first player
if (ballY>=454 && ballX<(player1X+74) && ballX>(player1X+70) && (!movingRight) )
{
	movingUp = false;
	movingRight=true;
	if (contPlay1<6)
	contPlay1++;
}

//up side collision with the second player  
if (ballX<=(1) && ballY >(player2Y-5) && ballY < (player2Y) && movingUp  )
{
	movingUp = false;
	movingRight=true;
	if (contPlay2<6)
	contPlay2++;
}

//down side collision with the second player
if (ballX<=(1) && ballY > (player2Y+70) && ballY<(player2Y+75) && (!movingUp) )
{
	movingUp = true;
	movingRight=true;
	if (contPlay2<6)
	contPlay2++;
}

// Left side collision with the third player 
if (ballY<=5 && ballX<(player3X) && ballX>(player3X-5) &&movingRight )
{
	movingUp = true;
	movingRight=false;
	if (contPlay3<6)
	contPlay3++;
}

//Right side collision with the third player
if (ballY<=5 && ballX<(player3X+74) && ballX>(player3X+70) && (!movingRight) )
{
	movingUp = true;
	movingRight=true;
	if (contPlay3<6)
	contPlay3++;
}

//up side collision with the fourth player         
if (ballX>=(485) && ballY > (player4Y-8) && ballY < (player4Y-7) && movingUp  )
{
	movingUp = false;
	movingRight=false;
	if (contPlay4<6 && !token)
	contPlay4++;
	token = false;
}

//down side collision with the fourth player     
if (ballX>=(485) && ballY > (player4Y+70) && ballY<(player4Y+75) && (!movingUp) )
{
	movingUp = true;
	movingRight=false;
	if (contPlay4<6 && !token)
	contPlay4++;
	token = false;
}


// The ball stroke with the player 1    
if(ballY>=450&& ballX>=(player1X-3) && ballX<=(player1X+70) )
{movingUp=false;
if (spin){
if (player1Left) { if(movingRight){right = right - 3;  left = left + 3;} else {right = right + 3;  left = left - 3;}  }
if (player1Right) {  if(movingRight){right = right + 3;  left = left - 3;} else {right = right - 3;  left = left + 3;}   }}
	}

// The ball stroke with the player 2
if(ballX<=(10) && ballY>=player2Y && ballY<=(player2Y+70))
{movingRight=true;
if (spin){
if (player2Up) { if(movingUp){up = up - 3;  down = down + 3;} else {up = up + 3;  down = down - 3;}  }
if (player2Down) { if(movingUp){up = up + 3;  down = down - 3;} else {up = up - 3;  down = down + 3;}  }}
	}
	
	// The ball stroke with the player 3
if(ballY<=10 && ballX>=player3X && (ballX-2)<=(player3X+70))
{movingUp=true;
if (spin){
if (player3Left) { if(movingRight){right = right - 3;  left = left + 3;} else {right = right + 3;  left = left - 3;}  }
if (player3Right) {  if(movingRight){right = right + 3;  left = left - 3;} else {right = right - 3;  left = left + 3;}   }}
	}
	
	// The ball stroke with the player 4   
if(ballX>=(478)&& ballY>=(player4Y-7) && ballY<=(player4Y+70) )
{movingRight=false;
if (spin){
if (player4Up) { if(movingUp){up = up - 3;  down = down + 3;} else {up = up + 3;  down = down - 3;}  }
if (player4Down) { if(movingUp){up = up + 3;  down = down - 3;} else {up = up - 3;  down = down + 3;}  }}
	}
	
	
}
}
}

}